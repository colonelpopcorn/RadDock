# A Radical Dock #

[![Build Status](https://gitlab.com/colonelpopcorn/RadDock/badges/master/pipeline.svg)](https://gitlab.com/colonelpopcorn/RadDock/-/pipelines)

RadDock is a program I made while working as a computer repair technician for a company that specializes in point-of-sale computers. My original idea was to make it easy for users to find common functions that they might use once a day, but not all the time. This way they wouldn't have to minimize whatever they were working on to find their application. It also alleviated my workload when a user couldn't find a specific application.

This is a more advanced version than the one that I originally developed, and I would like to improve this particular program with time. The biggest difference is where the program gets its path definitions. I created a very simple XML parser using MSDN documentation and persistence, and in the future I would like users to be able to write to this file. More persistence will be needed!

Below you'll find a roadmap that outlines where I want this project to go in the future. Think of them as miniature story cards.

<ul>
<li><strike>Make the browser definition cross reference with the path.xml file and all the browser/application definitions therein.</strike></li>
<li><strike>Make the dropdown in the tablegridview pre-populate with the right browser value.</strike></li>
<li><strike>Make the tablegridview an entry point for users to be able to edit each item definition.</strike></li>
<li><strike>Make another form for the user to be able to add argument-accepting application definitions.</strike></li>
</ul>

Version 1 is finished and ready for consumption. I still have a few things I want to accomplish, and there are a lot of unknowns but this can go on the back burner now.
